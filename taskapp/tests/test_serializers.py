from django.urls import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from ..serializers import *
from users.serializers import UserListSerializer, UserDetailSerializer


class TaskTest(APITestCase):
    def setUp(self):
        task = Task.objects.create(id="1",  title='first', description='first added')
        tag = Tag.objects.create(id="2",  title='second')

    @staticmethod
    def setup_user():
        User
        return User.objects.create_user(
            username='test',
            email='testuser@test.com',
            password='test'
        )

    def test_create_task(self):
        url = reverse('task_list_url')
        data = {'title': 'first title',
                'description': 'first description'} #, 'status': '2'
    
    
    def test_create_tag(self):
            url = reverse('tag_list_url')
            data = {'title': 'first title'}
