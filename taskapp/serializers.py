from rest_framework import serializers
from .models import Task, Tag



class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('id', 'author', 'title', 'description',
                  'status', 'created_at', 'finished_date', 'tags')


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'author', 'title', 'date', 'tasks')