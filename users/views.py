from django.contrib.auth import get_user_model
from rest_framework import generics
from .serializers import UserDetailSerializer, UserListSerializer
from rest_framework.permissions import IsAdminUser


class UserList(generics.ListAPIView):
    queryset = get_user_model().objects.all()
    serializer_class = UserListSerializer


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAdminUser,)
    queryset = get_user_model().objects.all()
    serializer_class = UserDetailSerializer